<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuestionSetsTable extends Migration {

    public function up() {
        Schema::create('question_sets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('admin_id');
            $table->string('title');
            $table->timestamps();

            $table
                ->foreign('admin_id')
                ->references('id')
                ->on('admins');
        });
    }

    public function down() {
        Schema::drop('question_sets');
    }
}
