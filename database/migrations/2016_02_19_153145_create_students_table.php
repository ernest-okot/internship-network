<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentsTable extends Migration {

    public function up() {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reg_id')->unique();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->unsignedInteger('lecturer_id');
            $table->unsignedInteger('admin_id');
            $table->rememberToken();
            $table->timestamps();

            $table
                ->foreign('admin_id')
                ->references('id')
                ->on('admins');

            $table
                ->foreign('lecturer_id')
                ->references('id')
                ->on('lecturers');
        });
    }

    public function down() {
        Schema::drop('students');
    }
}
