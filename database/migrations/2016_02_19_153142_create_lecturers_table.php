<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLecturersTable extends Migration {

    public function up() {
        Schema::create('lecturers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reg_id')->unique();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->unsignedInteger('admin_id');
            $table->rememberToken();
            $table->timestamps();

            $table
                ->foreign('admin_id')
                ->references('id')
                ->on('admins');
        });
    }

    public function down() {
        Schema::drop('lecturers');
    }
}
