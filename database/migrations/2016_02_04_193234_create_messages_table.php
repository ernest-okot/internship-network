<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessagesTable extends Migration {

    public function up() {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sender_type');
            $table->unsignedInteger('sender_id');
            $table->string('receiver_type');
            $table->unsignedInteger('receiver_id');
            $table->string('type');
            $table->string('content');
            $table->string('attachment')->nullable();
            $table->timestamps();

            $table->index(['sender_type', 'sender_id']);

            $table->index(['receiver_type', 'receiver_id']);
        });
    }

    public function down() {
        Schema::drop('messages');
    }
}
