<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuestionsTable extends Migration {

    public function up() {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('question_set_id');
            $table->text('content');
            $table->timestamps();

            $table
                ->foreign('question_set_id')
                ->references('id')
                ->on('question_sets');
        });
    }

    public function down() {
        Schema::drop('questions');
    }
}
