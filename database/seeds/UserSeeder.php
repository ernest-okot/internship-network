<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {

    public function run() {
        $a = new App\Admin();
        $a->reg_id = 'a56bgs@admins';
        $a->firstname = 'Ernest';
        $a->lastname = 'Okot';
        $a->email = 'okot08@gmail.com';
        $a->password = bcrypt('football');
        $a->save();

        $b = new App\Lecturer();
        $b->reg_id = 'a56232@lecturers';
        $b->firstname = 'Victor';
        $b->lastname = 'Olara';
        $b->email = 'vic.olara@gmail.com';
        $b->password = bcrypt('claire');
        $b->admin_id = $a->id;
        $b->save();

        $c = new App\Student();
        $c->reg_id = 'a56232@students';
        $c->firstname = 'Claire';
        $c->lastname = 'Adeke';
        $c->email = 'claire.nambi@gmail.com';
        $c->password = bcrypt('victor');
        $c->lecturer_id = $b->id;
        $c->admin_id = $a->id;
        $c->save();
    }
}
