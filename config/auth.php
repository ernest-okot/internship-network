<?php

return [

    'defaults' => [
        'guard'     => 'admin',
        'passwords' => 'users',
    ],

    'guards' => [
        'admin'    => [
            'driver'   => 'session',
            'provider' => 'admins',
        ],

        'lecturer' => [
            'driver'   => 'session',
            'provider' => 'lecturers',
        ],

        'student'  => [
            'driver'   => 'session',
            'provider' => 'students',
        ],

        'api' => [
            'driver'   => 'token',
            'provider' => 'users',
        ],
    ],

    'providers' => [

        'students' => [
            'driver' => 'eloquent',
            'model'  => App\Student::class,
        ],

        'lecturers' => [
            'driver' => 'eloquent',
            'model'  => App\Lecturer::class,
        ],

        'admins' => [
            'driver' => 'eloquent',
            'model'  => App\Admin::class,
        ],
    ],

    'passwords' => [
        'students'  => [
            'provider' => 'students',
            'email'    => 'auth.emails.password',
            'table'    => 'student_resets',
            'expire'   => 60,
        ],
        'lecturers' => [
            'provider' => 'lecturers',
            'email'    => 'auth.emails.password',
            'table'    => 'lecturer_resets',
            'expire'   => 60,
        ],
        'admins'    => [
            'provider' => 'admins',
            'email'    => 'auth.emails.password',
            'table'    => 'admin_resets',
            'expire'   => 60,
        ],
    ],

];
