<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Programme
 *
 * @property integer $id
 * @property string $title
 * @property string $start
 * @property string $end
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Programme whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Programme whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Programme whereStart($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Programme whereEnd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Programme whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Programme whereUpdatedAt($value)
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @property integer $admin_id
 * @method static \Illuminate\Database\Query\Builder|\App\Programme whereAdminId($value)
 * @mixin \Eloquent
 * @mixin \Eloquent
 */
class Programme extends Model {

    public $dates = ['created_at', 'update_at', 'start', 'end'];

    public function students() {
        return $this->belongsToMany(Student::class, 'programme_students');
    }
}
