<?php

namespace App;

use Eloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Admin
 *
 * @property integer $id
 * @property string $reg_id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $password
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\QuestionSet[] $questionSets
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Lecturer[] $lecturers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Log[] $logs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property string $remember_token
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Image[] $images
 * @method static \Illuminate\Database\Query\Builder|\App\Admin whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admin whereRegId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admin whereFirstname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admin whereLastname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admin whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admin wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admin whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Admin whereUpdatedAt($value)
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Programme[] $programmes
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 */
class Admin extends Eloquent implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract {
    use Authenticatable, Authorizable, CanResetPassword;

    public function programmes() {
        return $this->hasMany(Programme::class);
    }

    public function logs() {
        return $this->morphMany(Log::class, 'user');
    }

    public function messages() {
        return $this->morphMany(Message::class, 'sender');
    }

    public function questionSets() {
        return $this->hasMany(QuestionSet::class);
    }

    public function lecturers() {
        return $this->hasMany(Lecturer::class);
    }

    public function students() {
        return $this->hasMany(Student::class);
    }

    public function images() {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function conversation($user_id, $user_type) {
        return Message::where('sender_id', $this->id)
            ->where('sender_type', self::class)
            ->where('receiver_id', $user_id)
            ->where('receiver_type', $user_type)
            ->orWhere('sender_id', $user_id)
            ->where('sender_type', $user_type)
            ->where('receiver_id', $this->id)
            ->where('receiver_type', self::class)
            ->get();
    }

    public function avatar() {
        /** @var \App\Image $avatar */
        $avatar = $this->images()->orderBy('created_at', 'desc')->first();
        return $avatar ? $avatar->path : Image::DEFAULT_AVATAR;
    }

}
