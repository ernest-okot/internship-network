<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Student
 *
 * @property integer                                                      $id
 * @property string                                                       $reg_id
 * @property string                                                       $firstname
 * @property string                                                       $lastname
 * @property string                                                       $email
 * @property string                                                       $password
 * @property integer                                                      $lecturer_id
 * @property integer                                                      $admin_id
 * @property \Carbon\Carbon                                               $created_at
 * @property \Carbon\Carbon                                               $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Log[]     $logs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read \App\Lecturer                                           $lecturer
 * @property-read \App\Admin                                              $admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Image[]   $images
 * @property string                                                       $remember_token
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Answer[] $answers
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereRegId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereFirstname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereLastname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereLecturerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereAdminId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereUpdatedAt($value)
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 */
class Student extends Authenticatable {

    public function name() {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function logs() {
        return $this->morphMany(Log::class, 'user');
    }

    public function images() {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function conversation($user_id, $user_type) {
        return Message::where('sender_id', $this->id)
            ->where('sender_type', self::class)
            ->where('receiver_id', $user_id)
            ->where('receiver_type', $user_type)
            ->orWhere('sender_id', $user_id)
            ->where('sender_type', $user_type)
            ->where('receiver_id', $this->id)
            ->where('receiver_type', self::class)
            ->get();
    }

    public function programmes() {
        return $this->belongsToMany(Programme::class, 'programme_students');
    }

    public function onProgramme() {
        return $this->programmes()->where('end', '>', date('Y-m-d'))->count();
    }

    public function countdown() {
        return $this->programmes()
            ->where('end', '>', date('Y-m-d'))
            ->first()
            ->end
            ->diffInDays();
    }

    public function lecturer() {
        return $this->belongsTo(Lecturer::class);
    }

    public function admin() {
        return $this->belongsTo(Admin::class);
    }

    public function answers() {
        return $this->hasMany(Answer::class, 'user_id');
    }

    public function myLogs() {
        $logs = Log::query();
        return $logs
            ->where('user_id', $this->id)
            ->where('user_type', Student::class)
            ->orWhere('user_id', $this->lecturer_id)
            ->where('user_type', Lecturer::class)
            ->orWhere('user_id', $this->lecturer->admin_id)
            ->where('user_type', Admin::class)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function surveys() {
        return $this->admin->questionSets->filter(function(QuestionSet $survey) {
            return !$survey->questions->reduce(function($b, Question $q) {
                return $b + $q->answers()->where('user_id', $this->id)->count();
            }, 0);
        });
    }

    public function avatar() {
        /** @var \App\Image $avatar */
        $avatar = $this->images()->orderBy('created_at', 'desc')->first();
        return $avatar ? $avatar->path : Image::DEFAULT_AVATAR;
    }

}