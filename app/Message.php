<?php

namespace App;

use Eloquent;

/**
 * App\Message
 *
 * @property integer                                    $id
 * @property integer                                    $sender_id
 * @property integer                                    $receiver_id
 * @property string                                     $type
 * @property string                                     $content
 * @property \Carbon\Carbon                             $created_at
 * @property \Carbon\Carbon                             $updated_at
 * @property-read \App\Lecturer|\App\Admin|\App\Student $sender
 * @property-read \App\Lecturer|\App\Admin|\App\Student $receiver
 * @property string                                     $sender_type
 * @property string                                     $receiver_type
 * @property integer                                    $conversation_id
 * @property string                                     $attachment
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereSenderType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereSenderId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereReceiverType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereReceiverId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereAttachment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereUpdatedAt($value)
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 */
class Message extends Eloquent {

    public function sender() {
        return $this->morphTo();
    }

    public function receiver() {
        return $this->morphTo();
    }

    /**
     * @param \App\Student|\App\Lecturer|\App\Admin $user
     * @return string
     */
    public function meOrYouOrThem($user) {
        return $this->receiver_id === $user->id && $this->receiver_type === $user->getMorphClass() ?
            'paper bubble-me' :
            'leaf bubble-you';
    }

}
