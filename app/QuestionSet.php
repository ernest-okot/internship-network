<?php

namespace App;

use Eloquent;

/**
 * App\QuestionSet
 *
 * @property integer                                                       $id
 * @property integer                                                       $admin_id
 * @property string                                                        $title
 * @property \Carbon\Carbon                                                $created_at
 * @property \Carbon\Carbon                                                $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Question[] $questions
 * @property-read \App\Admin                                               $admin
 * @method static \Illuminate\Database\Query\Builder|\App\QuestionSet whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\QuestionSet whereAdminId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\QuestionSet whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\QuestionSet whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\QuestionSet whereUpdatedAt($value)
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 */
class QuestionSet extends Eloquent {

    public function questions() {
        return $this->hasMany(Question::class);
    }

    public function admin() {
        return $this->belongsTo(Admin::class);
    }
}
