<?php

namespace App;

use Eloquent;

/**
 * App\Log
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $user_type
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Lecturer|\App\Student|\App\Admin $user
 * @property string $attachment
 * @method static \Illuminate\Database\Query\Builder|\App\Log whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Log whereUserType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Log whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Log whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Log whereAttachment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Log whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Log whereUpdatedAt($value)
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 */
class Log extends Eloquent {

    public function user() {
        return $this->morphTo();
    }

    public function meOrYouOrThem() {
        $role = $this->user;
        return $role instanceof Student ?
            'paper bubble-me' :
            ($role instanceof Lecturer ?
                'leaf bubble-you' :
                'wall bubble-them');
    }
}
