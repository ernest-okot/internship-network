<?php

namespace App;

use Eloquent;

/**
 * App\Answer
 *
 * @property integer        $id
 * @property integer        $question_id
 * @property integer        $user_id
 * @property integer        $score
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Answer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Answer whereQuestionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Answer whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Answer whereScore($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Answer whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Answer whereUpdatedAt($value)
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 */
class Answer extends Eloquent {}
