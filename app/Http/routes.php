<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('layouts.main');
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/auth/login', 'Auth\AuthController@getLogin');

    Route::get('/auth/logout', 'Auth\AuthController@getLogout');

    Route::post('/auth/login', 'Auth\AuthController@postLogin');

    Route::get('/auth/settings', 'Auth\AuthController@getSettings');

    Route::post('/auth/settings/avatar', 'Auth\AuthController@postAvatar');

    Route::post('/auth/settings/password', 'Auth\AuthController@postPassword');


    // Admin Routes
    Route::group([
        'prefix'     => 'admin',
        'middleware' => 'auth:admin',
    ], function () {

        Route::get('/', function () {
            return redirect('/admin/logs');
        });

        Route::resource('logs', 'Admin\LogController');

        Route::get('/students/upload', 'Admin\StudentController@getUpload');

        Route::post('/students/upload', 'Admin\StudentController@postUpload');

        Route::resource('students', 'Admin\StudentController');

        Route::post('/students/{id}/programmes', 'Admin\StudentController@assignProgramme');

        Route::resource('lecturers', 'Admin\LecturerController');

        Route::get('/messages', 'Admin\MessageController@getIndex');

        Route::get('/messages/{id}', 'Admin\MessageController@getLecturerMessages');

        Route::post('/messages/{id}', 'Admin\MessageController@postLecturerMessages');

        Route::resource('surveys', 'Admin\SurveyController');

        Route::resource('programmes', 'Admin\ProgrammeController');

    });

    // Lecturer Routes
    Route::group([
        'prefix'     => 'lecturer',
        'middleware' => 'auth:lecturer',
    ], function () {

        Route::get('/', function () {
            return redirect('/lecturer/students');
        });

        Route::resource('logs', 'Lecturer\LogController');

        Route::get('/messages', 'Lecturer\MessageController@getIndex');

        Route::get('/messages/lecturer', 'Lecturer\MessageController@getAdminMessages');

        Route::get('/messages/{id}', 'Lecturer\MessageController@getStudentMessages');

        Route::post('/messages/lecturer', 'Lecturer\MessageController@postAdminMessages');

        Route::post('/messages/{id}', 'Lecturer\MessageController@postStudentMessages');

        Route::resource('/students', 'Lecturer\StudentController');

    });

    // Student Routes
    Route::group([
        'prefix'     => 'student',
        'middleware' => 'auth:student',
    ], function () {

        Route::get('/', function () {
            return redirect('/student/logs');
        });

        Route::resource('logs', 'Student\LogController');

        Route::get('/messages', 'Student\MessageController@getIndex');

        Route::get('/messages/lecturer', 'Student\MessageController@getLecturerMessages');

        Route::get('/messages/{id}', 'Student\MessageController@getStudentMessages');

        Route::post('/messages/lecturer', 'Student\MessageController@postLecturerMessages');

        Route::post('/messages/{id}', 'Student\MessageController@postStudentMessages');

        Route::resource('surveys', 'Student\SurveyController');

    });

});