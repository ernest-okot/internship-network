<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Image;
use App\Log;
use App\Student;
use Auth;
use Illuminate\Http\Request;

class LogController extends Controller {

    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|Student
     *
     */
    protected $user;

    public function __construct() {
        $this->user = Auth::guard('student')->user();
    }


    public function index() {
        return view('student.logs.index',  [
            'logs' => $this->user->myLogs(),
            'lecturer' => $this->user->lecturer
        ]);
    }

    public function create() {
        return view('logs.create');
    }

    public function store(Request $request) {
        $this->validate($request, ['content' => 'required']);

        $log = new Log();
        $log->content = $request->get('content');

        if ($request->hasFile('attachment')) {
            $attachment = $request->file('attachment');

            $filename = 'attachment-' . time() . '.' . $attachment->getClientOriginalExtension();

            $attachment->move(public_path('/uploads/attachments/'), $filename);

            $log->attachment = $filename;
        }

        $this
            ->user
            ->logs()
            ->save($log);


        return redirect('/student/logs');
    }

    public function show($id) {
        return view('logs.show',  ['log' => Log::findOrFail($id)]);
    }

    public function edit($id) {
        $log = Log::findOrFail($id);

        return view('logs.edit',  ['log' => Log::findOrFail($id)]);
    }

    public function update(Request $request, $id) {
        $log = Log::findOrFail($id);
        $log->content = $request->request->get('content');
        $log->save();

        return redirect()->route('logs.show', ['id' => $id]);
    }

    public function destroy($id) {
        $log = Log::findOrFail($id);
        $log->destroy();

        return redirect()->route('logs.index');
    }
}
