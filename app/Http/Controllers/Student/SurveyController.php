<?php

namespace App\Http\Controllers\Student;

use App\Answer;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use Illuminate\Http\Request;

class SurveyController extends Controller {

    /**
     * @var \App\Student
     */
    protected $user;

    public function __construct() {
        $this->user = Auth::guard('student')->user();
    }

    public function index() {
        return view('student.feedback.index', ['surveys' => $this->user->surveys()]);
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'questions' => 'array|size:5',
        ]);

        foreach ($request->get('questions') as $question => $score) {
            $answer = new Answer();
            $answer->question_id = $question;
            $answer->score = (int) $score;
            $this->user->answers()->save($answer);
        }

        return redirect('/student/surveys');
    }
}
