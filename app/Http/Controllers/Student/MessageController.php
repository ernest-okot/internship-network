<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Lecturer;
use App\Message;
use App\Student;
use Auth;
use Illuminate\Http\Request;

class MessageController extends Controller {

    /**
     * @var Student
     *
     */
    protected $user;

    public function __construct() {
        $this->user = Auth::guard('student')->user();
    }


    public function getIndex() {
        return redirect('/student/messages/lecturer');
    }

    public function postStudentMessages(Request $request, $receiver_id) {
        $this->validate($request, ['content' => 'required']);

        $message = new Message();
        $message->content = $request->get('content');
        $message->sender_id = $this->user->id;
        $message->sender_type = $this->user->getMorphClass();
        $message->receiver_id = $receiver_id;
        $message->receiver_type = Student::class;
        $message->save();

        return redirect('/student/messages/' . $receiver_id);
    }

    public function postLecturerMessages(Request $request) {
        $this->validate($request, ['content' => 'required']);

        $message = new Message();
        $message->content = $request->get('content');
        $message->sender_id = $this->user->id;
        $message->sender_type = $this->user->getMorphClass();
        $message->receiver_id = $this->user->lecturer->id;
        $message->receiver_type = Lecturer::class;
        $message->save();

        return redirect('/student/messages/lecturer');
    }

    public function getLecturerMessages() {
        return view('student.messages.index',  [
            'students' => $this->user->lecturer->students,
            'messages' => $this->user->conversation($this->user->lecturer->id, Lecturer::class)
        ]);
    }

    public function getStudentMessages($id) {
        return view('student.messages.index',  [
            'students' => $this->user->lecturer->students,
            'messages' => $this->user->conversation($id, Student::class)
        ]);
    }
}
