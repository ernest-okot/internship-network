<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Lecturer;
use Auth;
use Illuminate\Http\Request;

class LecturerController extends Controller {

    /**
     * @var Admin
     */
    protected $user = null;

    public function __construct() {
        $this->user = Auth::guard('admin')->user();
    }

    public function index() {
        return view('admin.lecturers.index', [
            'lecturers' => $this->user->lecturers,
        ]);
    }

    public function create() {
        return view('admin.lecturers.create');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'reg_id'    => 'required',
            'firstname' => 'required|max:255',
            'lastname'  => 'required|max:255',
            'email'     => 'required|email',
            'password'  => 'required|max:255',
        ]);

        $lecturer = new Lecturer();
        $lecturer->reg_id = $request->get('reg_id');
        $lecturer->firstname = $request->get('firstname');
        $lecturer->lastname = $request->get('lastname');
        $lecturer->email = $request->get('email');
        $lecturer->password = bcrypt($request->get('password'));

        $this->user->lecturers()->save($lecturer);

        return redirect('/admin/lecturers');
    }

    public function show($id) {
        /** @var Lecturer $lecturer */
        $lecturer = $this->user->lecturers()->find($id);

        return view('logs.show', ['lecturer' => $lecturer]);
    }

    public function edit($id) {
        /** @var Lecturer $lecturer */
        $lecturer = $this->user->lecturers()->find($id);

        return view('admin.lecturers.edit', ['lecturer' => $lecturer]);
    }

    public function update(Request $request, $id) {
        /** @var Lecturer $lecturer */
        $lecturer = $this->user->lecturers()->find($id);
        $lecturer->fill($request->request->all());
        $lecturer->save();

        return redirect('/admin/lecturers');
    }

    public function destroy($id) {
        /** @var Lecturer $lecturer */
        $lecturer = $this->user->lecturers()->find($id);
        $lecturer->delete();

        return redirect('/admin/lecturers');
    }
}
