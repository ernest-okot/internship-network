<?php

namespace App\Http\Controllers\Admin;

use App\Question;
use App\QuestionSet;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SurveyController extends Controller{

    /**
     * @var \App\Admin
     */
    protected $user = null;

    public function __construct() {
        $this->user = Auth::guard('admin')->user();
    }

    public function index() {
        return view('admin.surveys.index', [
            'surveys' => $this->user->questionSets,
            'lecturers' => $this->user->lecturers
        ]);
    }

    public function create() {
        return view('admin.surveys.create');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'title'      => 'required',
            'questions'    => 'required|array'
        ]);

        $survey = new QuestionSet();
        $survey->title = $request->get('title');
        $survey->admin_id = $this->user->id;
        $survey->save();

        $survey->questions()->saveMany(array_map(function($question) {
            return new Question(['content' => $question]);
        }, $request->get('questions')));

        return redirect('/admin/surveys');
    }

    public function show($id) {
        /** @var Student $student */
        $student = $this->user->students()->findOrFail($id);

        return view('admin.students.show', ['student' => $student]);
    }

    public function edit($id) {
        /** @var Student $student */
        $student = $this->user->students()->findOrFail($id);

        return view('admin.students.edit', [
            'student'   => $student,
            'lecturers' => $this->user->lecturers,
        ]);
    }

    public function update(Request $request, $id) {
        /** @var Student $student */
        $student = $this->user->students()->findOrFail($id);
        $student->fill($request->request->all());
        $student->save();

        return redirect('/admin/students');
    }

    public function destroy($id) {
        //
    }
}
