<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Lecturer;
use App\Message;
use Auth;
use Illuminate\Http\Request;

class MessageController extends Controller {

    /**
     * @var \App\Admin
     *
     */
    protected $user;

    public function __construct() {
        $this->user = Auth::guard('admin')->user();
    }

    public function getIndex() {
        return redirect('/admin/messages/' . $this->user->lecturers()->first()->id);
    }

    public function postLecturerMessages(Request $request, $id) {
        $this->validate($request, ['content' => 'required']);

        $message = new Message();
        $message->content = $request->get('content');
        $message->sender_id = $this->user->id;
        $message->sender_type = $this->user->getMorphClass();
        $message->receiver_id = $id;
        $message->receiver_type = Lecturer::class;
        $message->save();

        return redirect('/admin/messages/' . $id);
    }

    public function getLecturerMessages($id) {
        return view('admin.messages.index',  [
            'lecturers' => $this->user->lecturers,
            'messages' => $this->user->conversation($id, Lecturer::class)
        ]);
    }
}
