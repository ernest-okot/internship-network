<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Lecturer;
use App\Programme;
use App\Student;
use Auth;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Collections\CellCollection;
use Maatwebsite\Excel\Readers\LaravelExcelReader;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use Validator;

class StudentController extends Controller {

    /**
     * @var \App\Admin
     */
    protected $user = null;

    public function __construct() {
        $this->user = Auth::guard('admin')->user();
    }

    public function index() {
        return view('admin.students.index', [
            'students' => $this->user->students,
        ]);
    }

    public function create() {
        return view('admin.students.create', [
            'lecturers' => $this->user->lecturers,
            'programmes' => $this->user->programmes,
        ]);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'reg_id'      => 'required',
            'lecturer_id' => 'required',
            'programme_id' => 'required|exists:programmes,id',
            'firstname'   => 'required|max:255',
            'lastname'    => 'required|max:255',
            'email'       => 'required|email',
            'password'    => 'required|max:255',
        ]);

        $student = new Student();
        $student->reg_id = $request->get('reg_id');
        $student->lecturer_id = $request->get('lecturer_id');
        $student->firstname = $request->get('firstname');
        $student->lastname = $request->get('lastname');
        $student->email = $request->get('email');
        $student->password = $request->get('password');

        $this->user->students()->save($student);

        if($request->has('programme_id') &&
            $programme = $this->user->programmes()->find($request->get('programme_id'))) {
            /** @var Programme $programme */
            $programme
                ->students()
                ->save($student);
        }

        return redirect('/admin/students');
    }

    public function getUpload() {
        $file = Excel::create('hello', function(LaravelExcelWriter $writer) {
            $writer->sheet('Students', function(LaravelExcelWorksheet $sheet) {
                $sheet
                    ->fromArray([
                        'reg_id',
                        'firstname',
                        'lastname',
                        'email',
                        'password',
                    ])
                    ->setAllBorders('thin');
            });
        });

        $file->save();

        return response()->download($file->storagePath . '/' . $file->getFileName() . '.xls');
    }

    public function postUpload(Request $request) {
        $this->validate($request, [
            'programme_id'   => 'exists:programmes,id',
            'lecturer_id'   => 'required|exists:lecturers,id',
            'upload'        => 'required'
        ]);

        if ($request->file('upload')->getClientMimeType() != 'application/vnd.ms-excel') {
            return redirect('/admin/students/create')->with('upload.error', 'Invalid file type');
        }

        $file = $request->file('upload');

        /** @var Lecturer $lecturer */
        $lecturer = Lecturer::find($request->get('lecturer_id'));

        $errors = new Collection();

        $reader = Excel::load($file->getRealPath());

        $students = $reader
            ->all()
            ->filter(function(CellCollection $row) use ($errors) {
                $validation = Validator::make($row->toArray(), [
                    "reg_id"        => 'required',
                    "firstname"     => 'required',
                    "lastname"      => 'required',
                    "email"         => 'required|email|unique:students,email',
                    "password"      => 'required',
                ]);

                if ($validation->fails()) {
                    $row->put('errors', $validation->errors()->toArray());
                    $errors->put(null, $row);

                    return false;
                }

                return true;
            })
            ->map(function(CellCollection $row) use ($lecturer) {
                $student = new Student();

                $student->reg_id = $row->get('reg_id');
                $student->lecturer_id = $lecturer->id;
                $student->admin_id = $this->user->id;
                $student->firstname = $row->get('firstname');
                $student->lastname = $row->get('lastname');
                $student->email = $row->get('email');
                $student->password = bcrypt($row->get('password'));

                $student->save();

                return  $student;
            });

        if($programme = $this->user->programmes()->find($request->get('programme_id'))) {
            /** @var Programme $programme */
            $programme
                ->students()
                ->saveMany($students);

            return redirect('/admin/students');
        }

        return view('admin.students.import', [
            'students' => $students,
            'failures' => $errors
        ]);

    }

    public function show($id) {
        /** @var Student $student */
        $student = $this->user->students()->findOrFail($id);

        return view('admin.students.show', ['student' => $student]);
    }

    public function edit($id) {
        /** @var Student $student */
        $student = $this->user->students()->findOrFail($id);

        return view('admin.students.edit', [
            'student'   => $student,
            'lecturers' => $this->user->lecturers,
            'programmes' => $this->user->programmes()->where('end', '>', date('Y-m-d'))->get(),
        ]);
    }

    public function update(Request $request, $id) {
        /** @var Student $student */
        $student = $this->user->students()->findOrFail($id);
        $student->fill($request->request->all());
        $student->save();

        return redirect('/admin/students');
    }

    public function assignProgramme(Request $request, $id) {
        $this->validate($request, [
            'programme_id' => 'required|exists:programmes,id'
        ]);

        if(($student = $this->user->students()->find($id)) &&
            ($programme = $this->user->programmes()->find($request->get('programme_id')))) {
            /** @var Programme $programme */
            $programme
                ->students()
                ->save($student);

            return redirect('/admin/students');
        } else {
            return redirect()->back();
        }

    }

    public function destroy($id) {
        /** @var Student $student */
        $student = $this->user->students()->findOrFail($id);
        $student->delete();

        return redirect('/admin/students');
    }
}
