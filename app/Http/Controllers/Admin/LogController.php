<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Log;
use Auth;
use Illuminate\Http\Request;

class LogController extends Controller {

    /**
     * @var Admin
     */
    protected $user = null;

    public function __construct() {
        $this->user = Auth::guard('admin')->user();
    }

    public function index() {
        return view('admin.logs.index',  ['logs' => $this->user->logs]);
    }

    public function create() {
        return view('admin.logs.create');
    }

    public function store(Request $request) {
        $log = new Log();
        $log->content = $request->get('content');

        if ($request->hasFile('attachment')) {
            $attachment = $request->file('attachment');

            $filename = 'attachment-' . time() . '.' . $attachment->getClientOriginalExtension();

            $attachment->move(public_path('/uploads/attachments/'), $filename);

            $log->attachment = $filename;
        }

        $this->user->logs()->save($log);

        return redirect('/admin/logs');
    }

    public function show($id) {
        return view('admin.logs.show',  ['log' => $this->user->logs()->find($id)]);
    }

    public function edit($id) {
        /** @var Log $log */
        $log = $this->user->logs()->find($id);

        return view('logs.edit',  ['log' => $log]);
    }

    public function update(Request $request, $id) {
        /** @var Log $log */
        $log = $this->user->logs()->find($id);
        $log->content = $request->request->get('content');
        $log->save();

        return redirect('/admin/logs');
    }

    public function destroy($id) {
        /** @var Log $log */
        $log = $this->user->logs()->find($id);
        $log->delete();

        return redirect('/admin/logs');
    }
}
