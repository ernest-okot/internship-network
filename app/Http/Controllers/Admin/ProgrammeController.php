<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Programme;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProgrammeController extends Controller {

    /**
     * @var \App\Admin
     */
    protected $user = null;

    public function __construct() {
        $this->user = Auth::guard('admin')->user();
    }

    public function index() {
        return view('admin.programmes.index', [
            'programmes' => $this->user->programmes()->orderBy('end', 'desc')->get(),
        ]);
    }

    public function create() {
        return view('admin.programmes.create');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'title'      => 'required',
            'start'     => 'required|date',
            'end'       => 'required|date'
        ]);

        $programme = new Programme();
        $programme->title = $request->get('title');
        $programme->start = Carbon::createFromFormat('Y-m-d', $request->get('start'));
        $programme->end = Carbon::createFromFormat('Y-m-d', $request->get('end'));

        $this->user->programmes()->save($programme);

        return redirect('/admin/programmes');
    }

    public function show($id) {
        /** @var Programme $programme */
        $programme = $this->user->programmes()->findOrFail($id);

        return view('admin.programmes.show', ['programme' => $programme]);
    }

    public function edit($id) {
        /** @var Programme $programme */
        $programme = $this->user->programmes()->findOrFail($id);

        return view('admin.programmes.edit', [
            'programme' => $programme,
        ]);
    }

    public function update(Request $request, $id) {
        /** @var Programme $student */
        $programme = $this->user->programmes()->findOrFail($id);
        $programme->fill($request->request->all());
        $programme->save();

        return redirect('/admin/programmes');
    }

    public function destroy($id) {
        /** @var Programme $programmes */
        $programmes = $this->user->programmes()->findOrFail($id);
        $programmes->delete();

        return redirect('/admin/programmes');
    }
}
