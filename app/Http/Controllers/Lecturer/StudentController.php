<?php

namespace App\Http\Controllers\Lecturer;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Lecturer;
use App\Log;
use App\Student;
use Auth;
use Illuminate\Http\Request;

class StudentController extends Controller {

    /**
     * @var Lecturer
     *
     */
    protected $user;

    public function __construct() {
        $this->user = Auth::guard('lecturer')->user();
    }


    public function index() {
        return view('lecturer.logs.index', [
            'logs'      => $this->user->logs()->orderBy('created_at', 'desc')->get(),
            'students'  => $this->user->students,
            'broadcast' => true,
        ]);
    }

    public function create() {
        return view('logs.create');
    }

    public function store(Request $request) {
        $this->validate($request, ['content' => 'required']);

        $log = new Log();
        $log->content = $request->get('content');

        if ($request->hasFile('attachment')) {
            $attachment = $request->file('attachment');

            $filename = 'attachment-' . time() . '.' . $attachment->getClientOriginalExtension();

            $attachment->move(public_path('/uploads/attachments/'), $filename);

            $log->attachment = $filename;
        }

        $this->user->logs()->save($log);

        return redirect('/lecturer/students');
    }

    public function show($id) {
        return view('lecturer.logs.index', [
            'logs'     => Student::find($id)->logs,
            'students' => $this->user->students,
            'broadcast' => false,
        ]);
    }

    public function edit($id) {
        $log = Log::findOrFail($id);

        return view('logs.edit', ['log' => Log::findOrFail($id)]);
    }

    public function update(Request $request, $id) {
        $log = Log::findOrFail($id);
        $log->content = $request->request->get('content');
        $log->save();

        return redirect()->route('logs.show', ['id' => $id]);
    }

    public function destroy($id) {
        $log = Log::findOrFail($id);
        $log->delete();

        return redirect()->route('logs.index');
    }
}
