<?php

namespace App\Http\Controllers\Lecturer;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Message;
use App\Student;
use Auth;
use Illuminate\Http\Request;

class MessageController extends Controller {

    /**
     * @var \App\Lecturer
     *
     */
    protected $user;

    public function __construct() {
        $this->user = Auth::guard('lecturer')->user();
    }


    public function getIndex() {
        return redirect('/lecturer/messages/admin');
    }

    public function postStudentMessages(Request $request, $receiver_id) {
        $this->validate($request, ['content' => 'required']);

        $message = new Message();
        $message->content = $request->get('content');
        $message->sender_id = $this->user->id;
        $message->sender_type = $this->user->getMorphClass();
        $message->receiver_id = $receiver_id;
        $message->receiver_type = Student::class;
        $message->save();

        return redirect('/lecturer/messages/' . $receiver_id);
    }

    public function postAdminMessages(Request $request) {
        $this->validate($request, ['content' => 'required']);

        $message = new Message();
        $message->content = $request->get('content');
        $message->sender_id = $this->user->id;
        $message->sender_type = $this->user->getMorphClass();
        $message->receiver_id = $this->user->lecturer->id;
        $message->receiver_type = Admin::class;
        $message->save();

        return redirect('/lecturer/messages/lecturer');
    }

    public function getAdminMessages() {
        return view('lecturer.messages.index',  [
            'students' => $this->user->students,
            'messages' => $this->user->conversation($this->user->admin->id, Admin::class)
        ]);
    }

    public function getStudentMessages($id) {
        return view('lecturer.messages.index',  [
            'students' => $this->user->students,
            'messages' => $this->user->conversation($id, Student::class)
        ]);
    }
}
