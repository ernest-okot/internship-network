<?php

namespace App\Http\Controllers\Auth;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Image;
use App\Lecturer;
use App\Student;
use Auth;
use Illuminate\Http\Request;

class AuthController extends Controller {

    public function getSettings() {
        return view('auth.settings');
    }

    public function getLogin() {
        return view('auth.login');
    }

    public function getLogout() {
        foreach (['student', 'lecturer', 'admin'] as $guard) {
            if (Auth::guard($guard)->check()) {
                Auth::guard($guard)->logout();
                break;
            }
        }
        return redirect('/auth/login');
    }

    public function postLogin(Request $request) {
        $this->validate($request, ['role' => 'required|in:admin,student,lecturer']);

        $role = $request->get('role');

        if (Auth::guard($role)->attempt($request->only(['email', 'password']))) {
            $user = Auth::guard($role)->user();
            return $this->redirectPath($user);
        } else {
            return redirect('/auth/login')->with('error', 'Unable to login');
        }
    }

    public function postAvatar(Request $request) {
        $this->validate($request, ['avatar' => 'required|mimes:jpeg,png,gif']);

        $avatar = $request->file('avatar');
        $filename = 'avatar-' . time() . '.' . $avatar->getClientOriginalExtension();

        $avatar->move(public_path('/uploads/avatars/'), $filename);

        $image = new Image();
        $image->path = $filename;

        foreach (['student', 'lecturer', 'admin'] as $guard) {
            if (Auth::guard($guard)->check()) {
                /** @var Student|Lecturer|Admin $user */
                $user = Auth::guard($guard)->user();
                $user->images()->save($image);
                break;
            }
        }

        return redirect('/auth/settings/');
    }

    public function postPassword(Request $request) {
        $this->validate($request, [
            'current'  => 'required',
            'password' => 'required|confirmed',
        ]);

        $current = $request->get('current');
        $password = $request->get('password');

        foreach (['student', 'lecturer', 'admin'] as $guard) {
            if (Auth::guard($guard)->check()) {
                /** @var Student|Lecturer|Admin $user */
                $user = Auth::guard($guard)->user();

                if (!Auth::guard($guard)->validate(['email' => $user->email, 'password' => $current,])) {
                    return redirect('/auth/settings/')->with('errors', ['Invalid password']);
                }

                $user->password = bcrypt($password);
            }

            return redirect('/auth/settings/');
        }

    }

    public function redirectPath($user) {

        if ($user instanceof Student) {
            return redirect('/student');
        }

        if ($user instanceof Lecturer) {
            return redirect('/lecturer');
        }

        if ($user instanceof Admin) {
            return redirect('/admin');
        }

        return redirect('/auth/login');

    }
}
