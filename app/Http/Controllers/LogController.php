<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Log;
use Illuminate\Http\Request;
use Auth;

class LogController extends Controller {

    public function index() {
        return view('logs.index',  ['logs' => Log::all()]);
    }

    public function create() {
        return view('logs.create');
    }

    public function store(Request $request) {
        $log = new Log();
        $log->content = $request->get('content');

        Auth::guard()->user()->logs()->save($log);

        return redirect('/logs');
    }

    public function show($id) {
        return view('logs.show',  ['log' => Log::findOrFail($id)]);
    }

    public function edit($id) {
        $log = Log::findOrFail($id);

        return view('logs.edit',  ['log' => Log::findOrFail($id)]);
    }

    public function update(Request $request, $id) {
        $log = Log::findOrFail($id);
        $log->content = $request->request->get('content');
        $log->save();

        return redirect()->route('logs.show', ['id' => $id]);
    }

    public function destroy($id) {
        $log = Log::findOrFail($id);
        $log->destroy();

        return redirect()->route('logs.index');
    }
}
