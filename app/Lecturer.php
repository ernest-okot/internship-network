<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Lecturer
 *
 * @property integer $id
 * @property string $reg_id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $password
 * @property integer $admin_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Log[] $logs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Image[] $images
 * @property string $remember_token
 * @property-read \App\Admin $admin
 * @method static \Illuminate\Database\Query\Builder|\App\Lecturer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lecturer whereRegId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lecturer whereFirstname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lecturer whereLastname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lecturer whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lecturer wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lecturer whereAdminId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lecturer whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lecturer whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Lecturer whereUpdatedAt($value)
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 */
class Lecturer extends Authenticatable {

    public function name() {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function students() {
        return $this->hasMany(Student::class);
    }

    public function logs() {
        return $this->morphMany(Log::class, 'user');
    }

    public function images() {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function admin() {
        return $this->belongsTo(Admin::class);
    }

    public function avatar() {
        /** @var \App\Image $avatar */
        $avatar = $this->images()->orderBy('created_at', 'desc')->first();
        return $avatar ? $avatar->path : Image::DEFAULT_AVATAR;
    }

    /**
     * @param \App\QuestionSet $survey
     * @return mixed
     */
    public function score($survey) {
        $score = $this
            ->answers($survey)
            ->get()
            ->reduce(function($score, Answer $answer) {
                return $score + $answer->score;
            }, 0);

        $total = $this->students()->count() * 25;

        return $score ? $score * 100 / $total : 0;
    }

    public function engagement($survey) {
        $count = $this
            ->answers($survey)
            ->count();
        return ($count / 5) . "/" . $this->students()->count();
    }

    /**
     * @param $survey
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function answers($survey) {
        return Answer::whereIn('user_id', $this->students->map(function(Student $s) { return $s->id; }))
            ->whereIn('question_id', $survey->questions->map(function(Question $q) { return $q->id; }));
    }

    public function conversation($user_id, $user_type) {
        return Message::where('sender_id', $this->id)
            ->where('sender_type', self::class)
            ->where('receiver_id', $user_id)
            ->where('receiver_type', $user_type)
            ->orWhere('sender_id', $user_id)
            ->where('sender_type', $user_type)
            ->where('receiver_id', $this->id)
            ->where('receiver_type', self::class)
            ->orderBy('created_at', 'desc')
            ->get();
    }
}
