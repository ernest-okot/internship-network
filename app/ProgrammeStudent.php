<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ProgrammeStudent
 *
 * @property integer $id
 * @property integer $programme_id
 * @property integer $student_id
 * @property string $company_name
 * @property string $location
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\ProgrammeStudent whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProgrammeStudent whereProgrammeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProgrammeStudent whereStudentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProgrammeStudent whereCompanyName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProgrammeStudent whereLocation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProgrammeStudent whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProgrammeStudent whereUpdatedAt($value)
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 * @mixin \Eloquent
 */
class ProgrammeStudent extends Model
{
    //
}
