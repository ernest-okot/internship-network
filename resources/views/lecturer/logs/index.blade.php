@extends('layouts.main')

@section('content')

    <div class="grid">

        <div class="c-3">
            <div>
                <div class="leaf">
                    <a class="text-white" href="{{ url('/lecturer/students') }}">Broadcasts</a>
                </div>

                @foreach($students as $student)
                    <div class="paper">
                        <a class="text-white"
                           href="{{ url('/lecturer/students/' . $student->id) }}">{{ $student->name() }}</a>

                        <img class="left p-r-5"
                             src="/uploads/avatars/{{ $student->avatar() }}"
                             width="40" style="margin: -10px 0 0 -10px"/>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="c-6">

            @if ($broadcast)

                <form class="paper" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div>
                        <textarea name="content" id="" cols="80" rows="8" placeholder="Type your broadcast"></textarea>
                    </div>
                    <div><input type="file" name="attachment"></div>
                    <div>
                        <button type="submit">Save</button>
                    </div>
                </form>

            @endif

            @foreach($logs as $log)

                <div class="{{ $log->meOrYouOrThem() }}">

                    <img class="left p-r-5"
                         src="/uploads/avatars/{{ $log->user->avatar() }}"
                         width="40" style="margin: -10px 0 0 -10px"/>

                    <div class="p-l 5">
                        @if ($log->attachment)
                            <div class="right"><a download
                                                  href="{{ asset('/uploads/attachments/' . $log->attachment ) }}">attachment</a>
                            </div>
                        @endif
                        <div>{{ $log->created_at->format('l, jS F Y') }}</div>
                        <div class="m-t-10" style="white-space: pre-wrap">{{ $log->content }}</div>
                    </div>
                </div>
            @endforeach

            @if (!$logs->count())
                <div class="text-center text-white">No Logs</div>
            @endif

        </div>

    </div>

@endsection