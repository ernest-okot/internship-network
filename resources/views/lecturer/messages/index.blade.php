@extends('layouts.three')

@section('left')

    <div class="leaf">
        <a class="text-white" href="{{ url('/lecturer/messages/admin') }}">

            <img class="left p-r-5"
                 src="/uploads/avatars/{{ Auth::guard('lecturer')->user()->admin->avatar() }}"
                 width="50" style="margin: -10px 0 0 -10px"/>
            <span>Administrator</span>
        </a>
    </div>

    @foreach($students as $student)
        <div class="paper">
            <a class="text-white" href="{{ url('/lecturer/messages/' . $student->id) }}">{{ $student->name() }}</a>

            <img class="left p-r-5"
                 src="/uploads/avatars/{{ $student->avatar() }}"
                 width="40" style="margin: -10px 0 0 -10px"/>
        </div>
    @endforeach

@endsection

@section('main')
    <form class="paper composer" method="post">
        {{ csrf_field() }}
        <div>
            <textarea name="content" id="" cols="80" rows="8" placeholder="Type your message"></textarea>
        </div>
        <div>
            <button type="submit">Send</button>
        </div>
    </form>

    @foreach($messages as $message)
        <div class="{{ $message->meOrYouOrThem(Auth::guard('lecturer')->user()) }}">
            <div>{{ $message->created_at->diffForHumans() }}</div>
            <div style="white-space: pre-wrap">{{ $message->content }}</div>
        </div>
    @endforeach

    @if (!count($messages))
        <div class="text-center text-white">No Messages</div>
    @endif
@endsection