@extends('layouts.three')

@section('left')

    <div class="leaf">
        <a class="text-white" href="{{ url('/student/messages/lecturer') }}">Lecturer</a>
    </div>

    @foreach($students as $student)
        @if ($student->id !== Auth::guard('student')->user()->id)
            <div class="paper">
                <a class="text-white" href="{{ url('/student/messages/' . $student->id) }}">{{ $student->name() }}</a>
            </div>
        @endif
    @endforeach

@endsection

@section('main')


    <form class="paper composer" method="post">
        {{ csrf_field() }}
        <div>
            <textarea name="content" id="" cols="80" rows="8" placeholder="Type your message"></textarea>
        </div>
        <div>
            <button type="submit">Send</button>
        </div>
    </form>

    @foreach($messages as $message)
        <div class="{{ $message->meOrYouOrThem(Auth::guard('student')->user()) }}">
            <div>{{ $message->created_at->diffForHumans() }}</div>
            <div style="white-space: pre-wrap">{{ $message->content }}</div>
        </div>
    @endforeach

    @if (!$messages->count())
        <div class="text-center text-white">No Messages</div>
    @endif
@endsection