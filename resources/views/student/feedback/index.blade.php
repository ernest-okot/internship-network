@extends('layouts.main')

@section('content')

    <div class="grid">

        <div class="c-3"></div>

        <div class="c-6">

            @foreach($surveys as $survey)
                <form class="paper" action="/student/surveys/{{ $survey->id }}" method="post">
                    <input type="hidden" name="_method" value="put">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div>{{ $survey->title }}</div>
                    <ol>
                        @foreach($survey->questions as $question)
                            <li>
                                <div>{{ $question->content }}</div>
                                <div>
                                    <div><input type="checkbox" name="questions[{{ $question->id }}]" value="5">
                                        100%
                                    </div>
                                    <div><input type="checkbox" name="questions[{{ $question->id }}]" value="4"> 80%
                                    </div>
                                    <div><input type="checkbox" name="questions[{{ $question->id }}]" value="3"> 60%
                                    </div>
                                    <div><input type="checkbox" name="questions[{{ $question->id }}]" value="2"> 40
                                    </div>
                                    <div><input type="checkbox" name="questions[{{ $question->id }}]" value="1"> 20%
                                    </div>
                                    <div><input type="checkbox" name="questions[{{ $question->id }}]" value="0"> 0
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ol>
                    <div>
                        <button type="submit">Submit</button>
                    </div>
                </form>
            @endforeach

            @if (!$surveys->count())
                <div class="text-center text-white">No Surveys</div>
            @endif

        </div>

    </div>

@endsection