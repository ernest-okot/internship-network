@extends('layouts.main')

@section('content')

    <div class="grid">

        <div class="c-3">
            <form class="paper" method="post" enctype="multipart/form-data"><!-- upload whatever file format-->
                {{ csrf_field() }}
                <div>
                    <textarea name="content" id="" cols="30" rows="10" placeholder="Type your log here"></textarea>
                </div>
                <div><input type="file" name="attachment"></div>
                <div>
                    <button type="submit">Save</button>
                </div>
            </form>
        </div>

        <div class="c-6">

            @foreach($logs as $log)

                <div class="{{ $log->meOrYouOrThem() }}">

                    <img class="left p-r-5"
                         src="/uploads/avatars/{{ $log->user->avatar() }}"
                         width="40" style="margin: -10px 0 0 -10px"/>

                    <div class="p-l 5">
                        @if ($log->attachment)
                            <div class="right"><a download
                                                  href="{{ asset('/uploads/attachments/' . $log->attachment ) }}">attachment</a>
                            </div>
                        @endif
                        <div>{{ $log->created_at->format('l, jS F Y') }}</div>
                        <div class="m-t-10" style="white-space: pre-wrap">{{ $log->content }}</div>
                    </div>
                </div>

            @endforeach

            @if (!$logs->count())
                <div class="text-center text-white">No Logs</div>
            @endif

        </div>


        <div class="c-3">
            <img src="{{ asset('/uploads/avatars/' . $lecturer->avatar()) }}" alt="" style="width: 100%">
            <h3>Supervisor: <i>{{ $lecturer->name() }}</i></h3>
        </div>

    </div>

@endsection