@extends('layouts.three')

@section('main')

    <div class="wall">


        <div>
            <div>
                @foreach ($errors->all() as $error)
                    <div>
                        <small><i>{{ $error }}</i></small>
                    </div>
                @endforeach
                @if (isset($login_error))
                    <div>
                        <small><i>{{ $login_error }}</i></small>
                    </div>
                @endif
            </div>
        </div>

        <table>
            <tr>
                <td colspan="2">Change Password</td>
            </tr>

            <form action="{{ url('/auth/settings/password') }}" method="post">
                {{ csrf_field() }}

                <tr>
                    <td>
                        <small>Current Password</small>
                    </td>
                    <td><input type="password" name="current"/></td>
                </tr>
                <tr>
                    <td>
                        <small>New Password</small>
                    </td>
                    <td><input type="password" name="password"/></td>
                </tr>
                <tr>
                    <td>
                        <small>Confirm Password</small>
                    </td>
                    <td><input type="password" name="password_confirmation"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button type="submit">Save</button>
                    </td>
                </tr>

            </form>

            <tr>
                <td colspan="2">Change Avatar</td>
            </tr>

            <form action="{{ url('/auth/settings/avatar') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <tr>
                    <td>
                        <small>Select File</small>
                    </td>
                    <td><input type="file" name="avatar"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button type="submit">Save</button>
                    </td>
                </tr>

            </form>

        </table>
    </div>

@endsection