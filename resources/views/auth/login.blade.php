@extends('layouts.main')

@section('content')

    <div class="grid">
        <div class="col-6 col-push-3">

            <form class="login" method="post">

                {{ csrf_field() }}

                <table>
                    <tr>
                        <td><label class="text-white" for="username">Email</label></td>
                        <td><input type="text" name="email" /></td>
                    </tr>
                    <tr>
                        <td><label class="text-white" for="password">Password</label></td>
                        <td><input type="password" name="password" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <select name="role">
                                <option disabled selected>Student/Lecturer/Admin?</option>
                                <option value="admin">Administrator</option>
                                <option value="lecturer">Lecturer</option>
                                <option value="student">Student</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="submit">Login</button></td>
                    </tr>
                </table>


            </form>

        </div>
    </div>

@endsection