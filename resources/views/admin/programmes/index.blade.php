@extends('layouts.admin')

@section('main')

    @foreach($programmes as $programme)
        <div class="paper">
            <div class="right">
                <a href="{{ url('admin/programmes/' . $programme->id . '/edit') }}">Edit</a>
            </div>
            <div>
                <h3 style="margin: 0">{{ $programme->title }} ({{ $programme->end->diffInDays(Carbon\Carbon::now()) }} days left)</h3>
                <small>{{ $programme->start->format('jS F Y') }} to {{ $programme->end->format('jS F Y') }}</small>

                <div class="m-t-5">{{ $programme->students()->count() }} Students</div>
            </div>

        </div>
    @endforeach

    @if(!$programmes->count())
        <div class="text-center text-white">No Programmes Yet</div>
    @endif

@endsection