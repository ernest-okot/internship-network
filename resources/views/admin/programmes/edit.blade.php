@extends('layouts.admin')

@section('main')

    <div class="leaf">

        <form method="post" action="{{  url('/admin/programmes/' . $programme->id) }}">

            {{ csrf_field() }}

            <h4>{{ $programme->title }}</h4>

            <table>

                <tr>
                    <td>Title</td>
                    <td><input type="text" name="title" value="{{ $programme->title }}" required></td>
                </tr>

                <tr>
                    <td>Start Date</td>
                    <td><input type="date" placeholder="YYYY-MM-DD" name="start" value="{{ $programme->start }}" required></td>
                </tr>

                <tr>
                    <td>End Date</td>
                    <td><input type="date" placeholder="YYYY-MM-DD" name="end" value="{{ $programme->end }}" required></td>
                </tr>

            </table>


            <div><button type="submit">Save</button></div>

        </form>

        <form method="post" action="{{ url('admin/programmes/' . $programme->id) }}">
            <input type="hidden" name="_method" value="delete">
            {{ csrf_field() }}
            <button type="submit">Delete</button>
        </form>

    </div>

@endsection