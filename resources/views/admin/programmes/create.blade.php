@extends('layouts.admin')

@section('main')

    <form class="leaf" method="post" action="{{  url('/admin/programmes') }}">

        {{ csrf_field() }}

        <h4>New Programme</h4>

        <table>

            <tr>
                <td>Title</td>
                <td><input type="text" name="title" required></td>
            </tr>

            <tr>
                <td>Start Date</td>
                <td><input type="date" placeholder="YYYY-MM-DD" name="start" required></td>
            </tr>

            <tr>
                <td>End Date</td>
                <td><input type="date" placeholder="YYYY-MM-DD" name="end" required></td>
            </tr>

        </table>


        <div><button type="submit">Save</button></div>
    </form>

@endsection