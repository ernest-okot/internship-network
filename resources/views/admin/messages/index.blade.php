@extends('layouts.three')

@section('left')
    @foreach($lecturers as $lecturer)
        <div class="paper">
            <a class="text-white" href="{{ url('/admin/messages/' . $lecturer->id) }}">{{ $lecturer->name() }}</a>


            <img class="left p-r-5"
                 src="/uploads/avatars/{{ $lecturer->avatar() }}"
                 width="40" style="margin: -10px 0 0 -10px"/>
        </div>
    @endforeach
@endsection

@section('main')
    <form class="paper composer" method="post">
        {{ csrf_field() }}
        <div>
            <textarea name="content" id="" cols="80" rows="8" placeholder="Type your message"></textarea>
        </div>
        <div>
            <button type="submit">Send</button>
        </div>
    </form>

    @foreach($messages as $message)
        <div class="{{ $message->meOrYouOrThem(Auth::guard('admin')->user()) }}">
            <div>{{ $message->created_at->diffForHumans() }}</div>
            <div style="white-space: pre-wrap">{{ $message->content }}</div>
        </div>
    @endforeach

    @if (!count($messages))
        <div class="text-center text-white">No Messages</div>
    @endif
@endsection