@extends('layouts.admin')

@section('main')

    @foreach($surveys as $survey)
        <div class="paper">
            <div><h2>{{ $survey->title }}</h2></div>

            <table style="width: 100%">
                <tr>
                    <th style="text-align: left;">Lecturers</th>
                    <th style="text-align: left;">Score</th>
                    <th style="text-align: left;">Engagement</th>
                </tr>
                @foreach($lecturers as $lecturer)
                    <tr>
                        <td>{{ $lecturer->name() }}</td>
                        <td>{{ $lecturer->score($survey) }}%</td>
                        <td>{{ $lecturer->engagement($survey) }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    @endforeach

@endsection