@extends('layouts.admin')

@section('main')

    <div class="paper">

        <form method="post" action="{{  url('/admin/surveys/' . $survey->id) }}">

            {{ csrf_field() }}

            <h4>{{ $survey->title }}</h4>

            <table>

                <tr>
                    <td>Title</td>
                    <td><input type="text" name="title" value="{{ $survey->title }}" required></td>
                </tr>

            </table>


            <div><button type="submit">Save</button></div>

        </form>

        <form method="post" action="{{ url('admin/surveys/' . $survey->id) }}">
            <input type="hidden" name="_method" value="delete">
            {{ csrf_field() }}
            <button type="submit">Delete</button>
        </form>

    </div>

@endsection