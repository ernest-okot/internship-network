@extends('layouts.admin')

@section('main')

    <div class="paper">

        <form method="post" action="{{  url('/admin/surveys') }}">

            {{ csrf_field() }}

            <h4>New Survey</h4>

            <table>

                <tr>
                    <td>Title</td>
                    <td><input type="text" name="title" required></td>
                </tr>

                <tr>
                    <td>Question 1</td>
                    <td><textarea name="questions[1]" cols="30" rows="3"></textarea></td>
                </tr>

                <tr>
                    <td>Question 2</td>
                    <td><textarea name="questions[2]" cols="30" rows="3"></textarea></td>
                </tr>

                <tr>
                    <td>Question 3</td>
                    <td><textarea name="questions[3]" cols="30" rows="3"></textarea></td>
                </tr>

                <tr>
                    <td>Question 4</td>
                    <td><textarea name="questions[4]" cols="30" rows="3"></textarea></td>
                </tr>

                <tr>
                    <td>Question 5</td>
                    <td><textarea name="questions[5]" cols="30" rows="3"></textarea></td>
                </tr>

            </table>


            <div><button type="submit">Save</button></div>

        </form>

    </div>

@endsection