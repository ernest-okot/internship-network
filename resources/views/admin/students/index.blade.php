@extends('layouts.admin')

@section('main')

    @foreach($students as $student)
        <div class="paper">


            <img class="left p-r-5"
                 src="/uploads/avatars/{{ $student->avatar() }}"
                 width="40" style="margin: -10px 0 0 -10px"/>

            <div class="p-l-5">
                <div class="right">
                    <a class="text-white" href="{{ url('admin/students/' . $student->id . '/edit') }}">Edit</a>
                </div>
                <div>
                    <span>{{ $student->name() }}</span>
                    @if($student->programmes()->count())
                        <span>({{ $student->programmes()->first()->end->diffInDays(Carbon\Carbon::now()) }} days left)</span>
                    @endif
                </div>
            </div>
        </div>
    @endforeach

@endsection