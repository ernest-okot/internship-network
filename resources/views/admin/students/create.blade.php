@extends('layouts.admin')

@section('main')

    <form class="paper" method="post" action="{{  url('/admin/students') }}">

        {{ csrf_field() }}

        <h4>New Student</h4>

        <table>

            <tr>
                <td>Registration Id</td>
                <td><input type="text" name="reg_id" required></td>
            </tr>

            <tr>
                <td>First Name</td>
                <td><input type="text" name="firstname" required></td>
            </tr>

            <tr>
                <td>Last Name</td>
                <td><input type="text" name="lastname" required></td>
            </tr>

            <tr>
                <td>Email</td>
                <td><input type="email" name="email" required></td>
            </tr>

            <tr>
                <td>Password</td>
                <td><input type="password" name="password" required></td>
            </tr>

            <tr>
                <td>Supervisor</td>
                <td>
                    <select name="lecturer_id">
                        <option disabled selected>Pick One</option>
                        @foreach($lecturers as $lecturer)
                            <option value="{{ $lecturer->id }}">{{ $lecturer->name() }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            <tr>
                <td>Programme</td>
                <td>
                    <select name="programme_id">
                        <option disabled selected>Pick One</option>
                        @foreach($programmes as $programme)
                            <option value="{{ $programme->id }}">{{ $programme->title }} ({{ $programme->start->format('jS F Y') }} to {{ $programme->end->format('jS F Y') }})</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            <tr>
                <td></td>
                <td>
                    <button type="submit">Save</button>
                </td>
            </tr>

        </table>
    </form>

    <form class="paper" method="post" action="{{ url('/admin/students/upload') }}" enctype="multipart/form-data">

        {{ csrf_field() }}

        <h4>Upload student list from excel sheet, download a template <a class="text-white" href="{{ url('/admin/students/upload') }}">here</a></h4>

        <table>

            <tr>
                <td>Supervisor</td>
                <td>
                    <select name="lecturer_id">
                        <option disabled selected>Pick One</option>
                        @foreach($lecturers as $lecturer)
                            <option value="{{ $lecturer->id }}">{{ $lecturer->name() }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            <tr>
                <td>Programme</td>
                <td>
                    <select name="programme_id">
                        <option disabled selected>Pick One</option>
                        @foreach($programmes as $programme)
                            <option value="{{ $programme->id }}">{{ $programme->title }} ({{ $programme->start->format('jS F Y') }} to {{ $programme->end->format('jS F Y') }})</option>
                        @endforeach
                    </select>
                </td>
            </tr>


            <tr>
                <td>Upload</td>
                <td><input type="file" name="upload" accept="application/vnd.ms-excel"/></td>
            </tr>
        </table>


        <div>
            <button type="submit">Upload</button>
        </div>
    </form>

@endsection