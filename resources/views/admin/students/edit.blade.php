@extends('layouts.admin')

@section('main')

    <div class="paper">

        <form method="post" action="{{  url('/admin/students/' . $student->id) }}">

            {{ csrf_field() }}

            <h4>{{ $student->name() }}</h4>

            <table>

                <tr>
                    <td>Registration Id</td>
                    <td><input type="text" name="reg_id" value="{{ $student->reg_id }}" required></td>
                </tr>

                <tr>
                    <td>First Name</td>
                    <td><input type="text" name="firstname" value="{{ $student->firstname }}" required></td>
                </tr>

                <tr>
                    <td>Last Name</td>
                    <td><input type="text" name="lastname" value="{{ $student->lastname }}" required></td>
                </tr>

                <tr>
                    <td>Email</td>
                    <td><input type="email" name="email" value="{{ $student->email }}" required></td>
                </tr>

                <tr>
                    <td>Supervisor</td>
                    <td>
                        <select name="lecturer_id">
                            @foreach($lecturers as $lecturer)
                                <option value="{{ $lecturer->id }}" {{ $lecturer->id == $student->lecurer_id ? "selected" : "" }}>
                                    {{ $lecturer->name() }}
                                </option>
                            @endforeach
                        </select>
                    </td>
                </tr>

            </table>


            <div><button type="submit">Save</button></div>

        </form>

        <form method="post" >
        </form>

    </div>

    <form class="paper" method="post" action="{{ url('admin/students/' . $student->id) }}">

        {{ csrf_field() }}

        <h4>Delete this student</h4>


        <input type="hidden" name="_method" value="delete">
        {{ csrf_field() }}

        <div>
            <button type="submit">Delete</button>
        </div>
    </form>

    <form class="paper" method="post" action="{{ url('admin/students/' . $student->id . '/programmes') }}">

        {{ csrf_field() }}

        <h4>Assign student programme</h4>

        @if($student->onProgramme())

            <table>
                <tr>
                    <td>Programme</td>
                    <td>
                        <select name="programme_id">
                            @foreach($programmes as $programme)
                                <option value="{{ $programme->id }}" {{ $lecturer->id == $student->lecurer_id ? "selected" : "" }}>{{ $programme->title }} ({{ $programme->start->format('jS F Y') }} to {{ $programme->end->format('jS F Y') }})</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
            </table>

            <div>
                <button type="submit">Assign</button>
            </div>

        @else
            <td colspan="2">Can't assign programme, {{ $student->name() }}  is currently attending {{ $student->programmes()->first()->title }}</td>
        @endif
    </form>

@endsection