@extends('layouts.admin')

@section('main')

    <div class="paper">

        {{ csrf_field() }}

        <h4>Import from excel sheet</h4>

        <table style="width: 100%">

            <thead>
            <tr>
                <th class="text-left" colspan="4">Imported ({{ $students->count() }})</th>
            </tr>
            <tr>
                <th class="text-left">Reg ID</th>
                <th class="text-left">Name</th>
                <th class="text-left">Email</th>
                <th class="text-left">Lecturer</th>
            </tr>
            </thead>

            <tbody>
            @foreach($students as $student)
                <tr>
                    <td>{{ $student->reg_id }}</td>
                    <td>{{ $student->name() }}</td>
                    <td>{{ $student->email }}</td>
                    <td>{{ $student->lecturer->name() }}</td>
                </tr>
            @endforeach
            </tbody>

        </table>

        <table class="m-t-20" style="width: 100%">

            <thead>
            <tr>
                <th class="text-left" colspan="4">Failed ({{ $failures->count() }})</th>
            </tr>
            <tr>
                <td>Reg ID</td>
                <td>Name</td>
                <td>Email</td>
                <td>Reason</td>

            </tr>
            </thead>

            <tbody>
            @foreach($failures as $failure)
                <tr>
                    <td>{{ $failure->reg_id }}</td>
                    <td>{{ $failure->firstname . ' ' . $failure->lastname }}</td>
                    <td>{{ $failure->email }}</td>
                    <td>
                        @foreach($failure->errors as $errors)
                            @foreach($errors as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        @endforeach
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>
    </div>

@endsection