@extends('layouts.admin')

@section('main')

    <form class="paper" method="post">
        {{ csrf_field() }}
        <div>
            <textarea name="content" id="" cols="80" rows="8" placeholder="Type your broadcast"></textarea>
        </div>
        {{--<div><input type="file" name="attachment"></div>--}}
        <div>
            <button type="submit">Save</button>
        </div>
    </form>

@endsection