@extends('layouts.admin')

@section('main')

    @foreach($lecturers as $lecturer)
        <div class="paper">
            <img class="left p-r-5"
                 src="/uploads/avatars/{{ $lecturer->avatar() }}"
                 width="40" style="margin: -10px 0 0 -10px"/>

            <div class="p-l-5">
                <div class="right">
                    <a href="{{ url('admin/lecturers/' . $lecturer->id . '/edit') }}">Edit</a>
                </div>
                <div>{{ $lecturer->name() }}</div>
            </div>
        </div>
    @endforeach

@endsection