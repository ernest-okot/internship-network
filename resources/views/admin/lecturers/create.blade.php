@extends('layouts.admin')

@section('main')

    <form class="leaf" method="post" action="{{  url('/admin/lecturers') }}">

        {{ csrf_field() }}

        <h4>New Lecturer</h4>

        <table>

            <tr>
                <td>Registration Id</td>
                <td><input type="text" name="reg_id" required></td>
            </tr>

            <tr>
                <td>First Name</td>
                <td><input type="text" name="firstname" required></td>
            </tr>

            <tr>
                <td>Last Name</td>
                <td><input type="text" name="lastname" required></td>
            </tr>

            <tr>
                <td>Email</td>
                <td><input type="email" name="email" required></td>
            </tr>

            <tr>
                <td>Password</td>
                <td><input type="password" name="password" required></td>
            </tr>

            <tr>
                <td></td>
                <td><button type="submit">Save</button></td>
            </tr>

        </table>
    </form>

@endsection