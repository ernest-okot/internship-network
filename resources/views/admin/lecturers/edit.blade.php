@extends('layouts.admin')

@section('main')

    <div class="leaf">

        <form method="post" action="{{  url('/admin/lecturers/' . $lecturer->id) }}">

            {{ csrf_field() }}

            <h4>{{ $lecturer->name() }}</h4>

            <table>

                <tr>
                    <td>Registration Id</td>
                    <td><input type="text" name="reg_id" value="{{ $lecturer->reg_id }}" required></td>
                </tr>

                <tr>
                    <td>First Name</td>
                    <td><input type="text" name="firstname" value="{{ $lecturer->firstname }}" required></td>
                </tr>

                <tr>
                    <td>Last Name</td>
                    <td><input type="text" name="lastname" value="{{ $lecturer->lastname }}" required></td>
                </tr>

                <tr>
                    <td>Email</td>
                    <td><input type="email" name="email" value="{{ $lecturer->email }}" required></td>
                </tr>

            </table>


            <div><button type="submit">Save</button></div>

        </form>

        <form method="post" action="{{ url('admin/lecturers/' . $lecturer->id) }}">
            <input type="hidden" name="_method" value="delete">
            {{ csrf_field() }}
            <button type="submit">Delete</button>
        </form>

    </div>

@endsection