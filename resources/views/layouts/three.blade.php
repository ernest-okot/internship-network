@extends('layouts.main')

@section('content')

    <div class="grid">

        <div class="c-3">
            <div>@yield('left')</div>
        </div>

        <div class="c-6">
            <div>@yield('main')</div>
        </div>

        <div class="c-3">
            <div>@yield('right')</div>
        </div>

    </div>

@endsection