<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>InternshipNetwork</title>
    <link rel="stylesheet" href="{{ asset('/assets/css/app.css') }}">
</head>
<body>

<div class="header">
    <div class="logo left">InternshipNet</div>

    @if (\Auth::guard('student')->check())

        <div class="right">
            <img src="/uploads/avatars/{{ Auth::guard('student')->user()->avatar() }}" height="40"/>
        </div>

        <ul class="menu right">
            <li><a href="/student/">Home</a></li>
            <li><a href="/student/messages">Inbox</a></li>
            <li><a href="/student/surveys">Feedback</a></li>
            <li><a href="/auth/settings">Settings</a></li>
            <li><a href="/auth/logout">Logout {{ Auth::guard('student')->user()->firstname }}</a></li>
            @if(Auth::guard('student')->user()->onProgramme())
            <li><a href="#">[{{ Auth::guard('student')->user()->countdown() }} Days]</a></li>
            @endif
        </ul>

    @elseif (\Auth::guard('lecturer')->check())

        <div class="right">
            <img src="/uploads/avatars/{{ Auth::guard('lecturer')->user()->avatar() }}" height="40"/>
        </div>

        <ul class="menu right">
            <li><a href="/lecturer/">Home</a></li>
            <li><a href="/lecturer/messages">Inbox</a></li>
            <li><a href="/auth/settings">Settings</a></li>
            <li><a href="/auth/logout">Logout {{ Auth::guard('lecturer')->user()->firstname }}</a></li>
        </ul>

    @elseif (\Auth::guard('admin')->check())

        <div class="right">
            <img src="/uploads/avatars/{{ Auth::guard('admin')->user()->avatar() }}" height="40"/>
        </div>

        <ul class="menu right">
            <li><a href="/admin">Home</a></li>
            <li><a href="/admin/messages">Inbox</a></li>
            <li><a href="/auth/settings">Settings</a></li>
            <li><a href="/auth/logout">Logout {{ Auth::guard('admin')->user()->firstname }}</a></li>
        </ul>

    @else
        <ul class="menu right">
            <li><a href="/auth/login">Login</a></li>
        </ul>
    @endif

</div>

<div>
    @yield('content')
</div>

</body>
</html>