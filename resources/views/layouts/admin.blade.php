@extends('layouts.three')

@section('left')

    <div class="leaf">
        <a class="text-white" href="{{ url('/admin/logs') }}">Broadcasts</a>
    </div>

    <div class="paper">
        <a class="text-white right" href="{{ url('/admin/programmes/create') }}">+ Add</a>
        <a class="text-white" href="{{ url('/admin/programmes/') }}">Programmes</a>
    </div>

    <div class="paper">
        <a class="text-white right" href="{{ url('/admin/students/create') }}">+ Add</a>
        <a class="text-white" href="{{ url('/admin/students/') }}">Students</a>
    </div>

    <div class="paper">
        <a class="text-white right" href="{{ url('/admin/lecturers/create') }}">+ Add</a>
        <a class="text-white" href="{{ url('/admin/lecturers/') }}">Lecturers</a>
    </div>

    <div class="paper">
        <a class="text-white right" href="{{ url('/admin/surveys/create') }}">+ Add</a>
        <a class="text-white" href="{{ url('/admin/surveys/') }}">Survey</a>
    </div>

@endsection