<?php

namespace Illuminate\Support {

    /**
     * Class Fluent
     * @package Illuminate\Support
     *
     * @see http://laravel.com/docs/master/schema
     *
     * @method Fluent index(mixed $key = null)                    Set INTEGER to UNSIGNED
     * @method Fluent unique(mixed $key = null)                    Set INTEGER to UNSIGNED
     * @method Fluent unsigned()                    Set INTEGER to UNSIGNED
     * @method Fluent nullable()                    Designate that the column allows NULL values
     * @method Fluent default(mixed $value)         Declare a default value for a column
     *
     * @method Fluent references(string $key)       Specifies the name of the foreign key constraint
     * @method Fluent on(string $table)             Specifies the table on which the constraint applies to
     * @method Fluent onDelete(string $action)      Specifies the action to happen on a DELETE
     * @method Fluent onUpdate(string $action)      Specifies the action to happen on an UPDATE
     */
    class Fluent {}
}